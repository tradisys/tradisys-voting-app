import axios from 'axios';
import config from '../config';

const mapArrayPropsToObject = (arr, keyField) => Object.assign(
  {}, ...arr.map(item => ({ [item[keyField]]: item.value })),
);

const ws = axios.create({
  baseURL: config.api.nodeUrl,
});

export const getVotingAssetBalance = async (addr = config.voting.address) => {
  const { data: { balance } } = await ws.get(
    `/assets/balance/${addr}/${config.voting.assetId}`,
  );

  return balance;
};

export const getHeight = async () => {
  const { data: { height } } = await ws.get(
    '/blocks/height',
  );

  return height;
};

export const getVotingData = async () => {
  const { data } = await ws.get(
    `/addresses/data/${config.voting.address}`,
  );
  const values = mapArrayPropsToObject(data, 'key');

  const voteFor = values[config.voting.keyFor];
  const voteAgainst = values[config.voting.keyAgainst];
  const voteDuration = values[config.voting.keyDuration];
  const voteStartHeight = values[config.voting.keyStartHeight];

  return {
    voteFor,
    voteAgainst,
    voteDuration,
    voteStartHeight,
  };
};
