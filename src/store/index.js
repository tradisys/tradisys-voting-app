import Vue from 'vue';
import Vuex from 'vuex';
import actions from './actions';
import mutations from './mutations';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    votingHeightStart: 0,
    votingDuration: 0,
    votingAssetBalance: 0,
    votingAssetFor: 0,
    votingAssetAgainst: 0,
    height: 0,
    errors: {
      dialogVisible: false,
      dialogMessage: '',
    },
  },
  getters: {
    votingHeightStart: state => state.votingHeightStart,
    votingDuration: state => state.votingDuration,
    votingAssetBalance: state => state.votingAssetBalance,
    votingAssetFor: state => state.votingAssetFor,
    votingAssetAgainst: state => state.votingAssetAgainst,
    height: state => state.height,
    errorShow: state => state.errors.dialogVisible,
    errorMessage: state => state.errors.dialogMessage,
  },
  mutations,
  actions,
});
