import { invokeScript } from '@waves/waves-transactions';
import * as Api from '../api';
import * as Actions from './types/actions';
import * as Mutations from './types/mutations';
import * as VotesTypes from '../types/votes';
import config from '../config';

export const wait = ms => new Promise(r => setTimeout(r, ms));

export default {
  async [Actions.LIFECYCLE]({ commit, dispatch }) {
    try {
      const [data, height, balance] = await Promise.all([
        Api.getVotingData(),
        Api.getHeight(),
        Api.getVotingAssetBalance(),
      ]);

      const {
        voteFor,
        voteAgainst,
        voteDuration,
        voteStartHeight,
      } = data;

      commit(Mutations.SET_VOTING_HEIGHT_START, voteStartHeight);
      commit(Mutations.SET_VOTING_DURATION, voteDuration);
      commit(Mutations.SET_VOTING_ASSET_FOR, voteFor);
      commit(Mutations.SET_VOTING_ASSET_AGAINST, voteAgainst);
      commit(Mutations.SET_VOTING_ASSET_BALANCE, balance);
      commit(Mutations.SET_HEIGHT, height);
    } finally {
      await wait(5000);
      dispatch(Actions.LIFECYCLE);
    }
  },
  async [Actions.VOTE]({ dispatch }, payload) {
    if (window.WavesKeeper === undefined) {
      dispatch(Actions.SET_ERROR_DIALOG_MESSAGE, 'Waves Keeper is not installed. Please install Waves Keeper first.<br><br><a target="_blank" href="https://wavesplatform.com/technology/keeper">Get Waves Keeper</a>');
      dispatch(Actions.SHOW_ERROR_DIALOG);
      return;
    }
    try {
      const WavesApi = await window.WavesKeeper.initialPromise;

      const {
        account: {
          address,
          publicKey,
          networkCode,
        },
      } = await WavesApi.publicState();

      if (networkCode !== config.api.chainId) {
        dispatch(Actions.SET_ERROR_DIALOG_MESSAGE, 'Switch to MAINNET please.');
        dispatch(Actions.SHOW_ERROR_DIALOG);
        return;
      }

      const assetBalance = await Api.getVotingAssetBalance(address);

      const invocation = invokeScript({
        senderPublicKey: publicKey,
        dApp: config.voting.address,
        chainId: config.api.chainId,
        call: {
          function: payload === VotesTypes.FOR ? 'voteFor' : 'voteAgainst',
          args: [],
        },
        payment: [{
          assetId: config.voting.assetId,
          amount: assetBalance,
        }],
        fee: 500000,
      });

      const tx = {
        type: 16,
        data: {
          ...invocation,
          fee: {
            assetId: 'WAVES',
            coins: 500000,
          },
        },
      };

      await WavesApi.signAndPublishTransaction(tx);
    } catch (e) {
      console.log(e);
      if (e.code && e.code === '12') {
        dispatch(Actions.SET_ERROR_DIALOG_MESSAGE, 'You\'ve rejected authentication probably. Please check your Waves Keeper permission settings for this website.');
        dispatch(Actions.SHOW_ERROR_DIALOG);
      } else if (e.code && e.code === '14') {
        dispatch(Actions.SET_ERROR_DIALOG_MESSAGE, 'Add Waves Keeper account.');
        dispatch(Actions.SHOW_ERROR_DIALOG);
      } else if (e.code && e.code === '15') {
        dispatch(Actions.SET_ERROR_DIALOG_MESSAGE, 'You don\'t have enough tokens to vote.');
        dispatch(Actions.SHOW_ERROR_DIALOG);
      } else {
        dispatch(Actions.SET_ERROR_DIALOG_MESSAGE, e.message);
        dispatch(Actions.SHOW_ERROR_DIALOG);
      }
    }
  },
  [Actions.SHOW_ERROR_DIALOG]({ commit }) {
    commit(Mutations.SET_ERROR_DIALOG_VISIBILITY, true);
  },
  [Actions.HIDE_ERROR_DIALOG]({ commit }) {
    commit(Mutations.SET_ERROR_DIALOG_VISIBILITY, false);
  },
  [Actions.SET_ERROR_DIALOG_MESSAGE]({ commit }, payload) {
    commit(Mutations.SET_ERROR_DIALOG_MESSAGE, payload);
  },
};
