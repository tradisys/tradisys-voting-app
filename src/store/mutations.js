import * as Mutations from './types/mutations';

export default {
  [Mutations.SET_VOTING_ASSET_BALANCE](state, payload) {
    state.votingAssetBalance = payload;
  },
  [Mutations.SET_VOTING_ASSET_FOR](state, payload) {
    state.votingAssetFor = payload;
  },
  [Mutations.SET_VOTING_ASSET_AGAINST](state, payload) {
    state.votingAssetAgainst = payload;
  },
  [Mutations.SET_VOTING_DURATION](state, payload) {
    state.votingDuration = payload;
  },
  [Mutations.SET_VOTING_HEIGHT_START](state, payload) {
    state.votingHeightStart = payload;
  },
  [Mutations.SET_HEIGHT](state, payload) {
    state.height = payload;
  },
  [Mutations.SET_ERROR_DIALOG_MESSAGE](state, payload) {
    state.errors.dialogMessage = payload;
  },
  [Mutations.SET_ERROR_DIALOG_VISIBILITY](state, payload) {
    state.errors.dialogVisible = payload;
  },
};
