export default {
  api: {
    nodeUrl: 'https://nodes.wavesnodes.com',
    chainId: 'W',
  },
  voting: {
    // primary
    // Voting dApp address:
    address: '3PKXfEQ6qcZAncrfjaNCMJnSPxkJ7CwmqBR',
    // Voting asset id:
    assetId: 'DCuF24BVUMtZofJexNVKaiHB3QvpnkyNwgeThfpS11kF',
    // Voting emission amount:
    assetEmissionAmount: 2571028,
    // keys:
    keyStartHeight: '$votingStartHeight',
    keyFor: '$voteFor',
    keyAgainst: '$voteAgainst',
    keyDuration: '$votingDuration',
  },
};
